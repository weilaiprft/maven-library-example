FROM open-liberty

ARG VERSION=1.0
ARG REVISION=SNAPSHOT

LABEL \
  org.opencontainers.image.authors="Wei Lai" \
  org.opencontainers.image.vendor="Perficient" \
  org.opencontainers.image.url="local" \
  org.opencontainers.image.source="https://github.com/OpenLiberty/guide-getting-started" \
  org.opencontainers.image.version="$VERSION" \
  org.opencontainers.image.revision="$REVISION" \
  vendor="Open Liberty" \
  name="trex5.5.1" \
  version="$VERSION-$REVISION" \
  summary="The system microservice from the Getting Started guide" \
  description="This image contains the system microservice running with the Open Liberty runtime."


COPY --chown=1001:0 TreXWorkspaceWeb/src/main/liberty/dockerconfig/ /config/
#COPY --chown=1001:0 --from=build /home/app/trex/TreXWorkspaceWeb/target/*.war /config/apps/
# COPY --chown=1001:0 TreXWorkspaceWeb/src/main/liberty/dockerconfig/ /config/
# COPY --chown=1001:0 target/*.war /config/apps/

RUN configure.sh