package io.cloudrepo.examples.maven.math;
// test ssh commit
// test again, can push with source tree....
//removed bitbucket ssh key from user setting
// add git bash generated keys
// opened vscode from git bash, working git ssh
// trying with code
// works from git bash to vscode to push
// having git bash ssh-agent running in the background
// open standalone vscode, trying to ssh push
// test with source tree with git bash running ssh agent
// test vscode push again
// now try mvn release steps

import com.filenet.api.core.*;

/**
 * Implement some basic math functions for use in applications.
 *
 * This class will be published as part of an example library and can be consumed as a dependency in application projects.
 */
public class Functions {


    /**
     * Add a series of integers.
     *
     * @param numbers the numbers you wish to add.
     * @return The mathematical sum (addition) of the provided numbers.
     */
    public static int add(int... numbers){

        int result = 0;
        for (int number : numbers) {
            result = number + result;
        }
        return result;
    }
}
