package io.cloudrepo.examples.maven.math;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for our mathematical functions.
 */
class FunctionsTest {

    @org.junit.jupiter.api.Test
    void add() {
        System.out.println("this is a system output");
        assertEquals(0, Functions.add(), "No numbers provided, sum should be zero.");
        assertEquals(25, Functions.add(1, 3, 5, 7, 9), "Adding a list of numbers should provide their sum.");
        assertEquals(0, Functions.add(-1, 1), "Adding negative numbers.");
    }
}